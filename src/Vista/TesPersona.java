/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Estudiante;

/**
 *
 * @author Lab06pcdocente
 */
public class TesPersona {

    public static void main(String[] args) {

        Estudiante luis = new Estudiante("Luis", 0220131053, 3, 3, 1);
        Estudiante carlos = new Estudiante("Carlos", 02220131051, 1, 1, 1);
        Estudiante madarme = new Estudiante("Madarme", 0220631051, 1, 1, 5);

        System.out.println(luis);
        System.out.println(carlos.toString());

        if (luis.equals(carlos)) {
            System.out.println("Igualitos");
        } else {
            System.out.println("No son igualitos");
        }

        if (luis.compareTo(carlos) == 0) {
            System.out.println("Iguales en su definitiva");
        } else if (luis.compareTo(carlos) > 0) {
            System.out.println("Luis tiene nota def mayor que carlos");
        } else {
            System.out.println("Luis tiene nota def menor que carlos");
        }
        
        ordenarEstudiantes(luis, carlos, madarme);
    }

    private static void ordenarEstudiantes(Estudiante a, Estudiante b, Estudiante c) {
        
        int ab=a.compareTo(b);
        int bc=b.compareTo(c);
        if (ab > 0) {
            // Si a es mayor que b, intercambiamos a y b
            Estudiante temp = a;
            a = b;
            b = temp;
        }
        if (bc > 0) {
            // Si b es mayor que c, intercambiamos b y c
            Estudiante temp = b;
            b = c;
            c = temp;
        }
        System.out.println(a.getNombre()+",Def:"+a.getDefinitiva());
        System.out.println(b.getNombre()+",Def:"+b.getDefinitiva());
        System.out.println(c.getNombre()+",Def:"+c.getDefinitiva());
        
        
    }
}
