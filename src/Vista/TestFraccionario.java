/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Fraccionario;

/**
 *
 * @author Lab05pcdocente
 */
public class TestFraccionario {

    public static void main(String[] args) {
        Fraccionario a = new Fraccionario(1, 2);
        Fraccionario b = new Fraccionario(1, 2);
        Fraccionario c = new Fraccionario(2, 7);
        ordenar(a, b, c);
    }

    private static void ordenar(Fraccionario a, Fraccionario b, Fraccionario c) {

        int ab = a.compareTo(b);
        int bc = b.compareTo(c);
        if (ab > 0) {
            // Si a es mayor que b, intercambiamos a y b
            Fraccionario temp = a;
            a = b;
            b = temp;
        }
        if (bc > 0) {
            // Si b es mayor que c, intercambiamos b y c
            Fraccionario temp = b;
            b = c;
            c = temp;
        }
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());

    }

}
