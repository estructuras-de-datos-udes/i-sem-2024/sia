/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.LinkedList;

/**
 *
 * @author madar
 */
public class ListadoEstudiante {

    //es una lista de estudiantes:
    private LinkedList<Estudiante> myLista = new LinkedList();

    public ListadoEstudiante() {
    }

    public LinkedList<Estudiante> getMyLista() {
        return myLista;
    }

    public void setMyLista(LinkedList<Estudiante> myLista) {
        this.myLista = myLista;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Estudiante estudiante : myLista) {

            msg += estudiante.toString() + "\t";

        }
        return msg;
    }

    public void insertar(Estudiante nuevo) {
        this.myLista.add(nuevo);
    }

    /**
     * Método retorna el estudiante con menor promedio
     *
     * @return
     */
    public Estudiante getMenor() {
        // menor = myLista[0]
        Estudiante menor = this.myLista.get(0);
        for (int i = 1; i < this.myLista.size(); i++) {
            Estudiante dos = this.myLista.get(i);
            int c = menor.compareTo(dos);
            // menor - dos 
            if (c > 0) {
                menor = dos;
            }
        }
        return menor;
    }

    /**
     * Retorna una lista de los menores estudianes
     * @return un listado
     */
    public ListadoEstudiante getMenores()
    {
        return null;
    }
}
